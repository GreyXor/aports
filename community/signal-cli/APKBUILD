# Contributor: Siva Mahadevan <me@svmhdvn.name>
# Contributor: Simon Frankenberger <simon-alpine@fraho.eu>
# Maintainer: Simon Frankenberger <simon-alpine@fraho.eu>
pkgname=signal-cli
pkgver=0.13.8
pkgrel=0
pkgdesc="commandline interface for libsignal-service-java"
url="https://github.com/AsamK/signal-cli"
# jdk17 only available on 64 bit archs
# cargo and rust not available on s390x and riscv64
# java-libsignal-client not available on ppc64le
arch="x86_64 aarch64"
license="GPL-3.0-or-later"
depends="java-libsignal-client"
makedepends="openjdk21-jdk"
source="
	$pkgname-$pkgver.tar.gz::https://github.com/AsamK/signal-cli/archive/v$pkgver.tar.gz
	"

build() {
	./gradlew installDist
}

check() {
	./gradlew check
}

package() {
	local buildhome="build/install/$pkgname"
	local installhome="/usr/share/java/$pkgname"

	install -dm755 "$pkgdir/$installhome/lib"
	install -m644 "$buildhome"/lib/* "$pkgdir/$installhome/lib"

	install -Dm755 "$buildhome/bin/$pkgname" "$pkgdir/$installhome/bin/$pkgname"
	install -dm755 "$pkgdir/usr/bin"
	ln -s "$installhome/bin/$pkgname" "$pkgdir/usr/bin/$pkgname"

	rm -v "$pkgdir/$installhome/lib"/libsignal-client-*.jar
	ln -sv /usr/share/java/libsignal-client/signal-client-java.jar "$pkgdir/$installhome/lib/signal-client-java.jar"
	sed -i -e 's/libsignal-client-[0-9.]\+\.jar/signal-client-java.jar/g' "$pkgdir/$installhome/bin/$pkgname"
}

sha512sums="
e26eb40aaee9380042ddfe6c08af6dfdedfc446573066623e2438e4711dbd8cb45571cf896a02c2d4de5b20c27f5b02e5741720625658558379f20809e1d54e6  signal-cli-0.13.8.tar.gz
"
